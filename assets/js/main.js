window.onload = function () {
	
	// Use XML GET request to get json data from api / link
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Parse data into json
			let data = JSON.parse(xhttp.responseText);

			let blocks = "";

			data.blocks.forEach(function (block) {
				let section = '<div class="accordion__section"><div class="accordion__header"><h3>' + block.heading + '</h3><img src="assets/icons/arrow.svg" alt="" /><div class="clear"></div></div><div class="accordion__content"><p>' + block.content + '</p></div></div>';

				blocks += section;
			});

			document.getElementById("accordion").innerHTML = blocks;

			let sections = document.getElementsByClassName("accordion__section");

			for (let i = 0; i < sections.length; i++) {
				let section = sections[i];
				let header = section.getElementsByClassName("accordion__header")[0];
				header.addEventListener("click", function () {
					section.classList.toggle("selected");

					for (let j = 0; j < sections.length; j++) {
						if (sections[j] != section) {
							sections[j].classList.remove("selected");
						}
					}
				});
			}
		}
	};
	xhttp.open("GET", "https://design.propcom.co.uk/buildtest/accordion-data.json", true);
	xhttp.send();
}